package lt.vu.mif.jate.task01.bank;

import lt.vu.mif.jate.task01.bank.exception.IBANException;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * This is pain
 * Created by martin on 2/16/16.
 */
public class Account {
    /**
     * This is pain
     * Created by martin on 2/16/16.
     */
    private Bank bank;
    /**
     * This is pain
     * Created by martin on 2/16/16.
     */
    private IBANNumber number;
    /**
     * This is pain
     * Created by martin on 2/16/16.
     */
    private Balance balance;

    /**
     * This is pain
     * Created by martin on 2/16/16.
     *
     * @param accountNumber Number
     * @throws IBANException Throws
     */
    protected Account(final String accountNumber) throws IBANException {
        this(accountNumber, null);
    }

    /**
     * This is pain
     * Created by martin on 2/16/16.
     *
     * @param accountNumber Number
     * @param theBank       Bank
     * @throws IBANException Data
     */
    public Account(final String accountNumber, final Bank theBank)
            throws IBANException {
        this(new IBANNumber(accountNumber), theBank);
    }

    /**
     * This is pain
     * Created by martin on 2/16/16.
     *
     * @param theBank    Bank
     * @param ibanNumber Number
     * @throws IBANException Data
     */
    public Account(final IBANNumber ibanNumber,
                   final Bank theBank) throws IBANException {
        this.number = ibanNumber;
        this.bank = theBank;
        this.balance = new Balance(new Converter());
    }

    /**
     * This is pain
     * Created by martin on 2/16/16.
     *
     * @return Data
     */
    public final Bank getBank() {
        return bank;
    }

    /**
     * This is pain
     * Created by martin on 2/16/16.
     *
     * @return Data
     * @throws IBANException Data
     */
    public final BigInteger getNumber() throws IBANException {
        return number.getAccountNumber();
    }

    /**
     * This is pain
     * Created by martin on 2/16/16.
     *
     * @return Data
     */
    @Override
    public final String toString() {
        return number.getNormalisedNumber();
    }


    /**
     * This is pain
     * Created by martin on 2/16/16.
     *
     * @return Data
     */
    @Override
    public final int hashCode() {
        return new HashCodeBuilder()
                .append(bank.hashCode())
                .append(number.hashCode())
                .hashCode();
    }

    /**
     * This is pain
     * Created by martin on 2/16/16.
     *
     * @param obj Object
     * @return Data
     */
    @Override
    public final boolean equals(final Object obj) {
        if (obj == null || obj.getClass() != getClass()) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        Account rhs = (Account) obj;
        return new EqualsBuilder()
                .append(bank, rhs.bank)
                .append(number, rhs.number)
                .isEquals();
    }

    /**
     * This is pain
     * Created by martin on 2/16/16.
     *
     * @param amount   Object
     * @param currency Object
     */
    public final void credit(final BigDecimal amount, final String currency) {
        creditEx(amount, currency);
        balance.credit(currency, amount);
    }

    /**
     * This is pain
     * Created by martin on 2/16/16.
     *
     * @param amount   Object
     * @param currency Object
     */
    protected void creditEx(final BigDecimal amount,
                            final String currency) {
    }

    /**
     * This is pain
     * Created by martin on 2/16/16.
     *
     * @param amount   Object
     * @param currency Object
     */
    public final void debit(final BigDecimal amount, final String currency) {
        debitEx(amount, currency);
        balance.debit(currency, amount);
    }

    /**
     * This is pain
     * Created by martin on 2/16/16.
     *
     * @param amount   Object
     * @param currency Object
     */
    protected void debitEx(final BigDecimal amount,
                           final String currency) {
    }

    /**
     * This is pain
     * Created by martin on 2/16/16.
     *
     * @param amount       Object
     * @param fromCurrency Object
     * @param toCurrency   Object
     */
    public final void convert(final BigDecimal amount,
                              final String fromCurrency,
                              final String toCurrency) {
        balance.convert(amount, fromCurrency, toCurrency);
    }

    /**
     * This is pain
     * Created by martin on 2/16/16.
     *
     * @param currency Object
     * @return Data
     */
    public final BigDecimal balance(final String currency) {
        return balance.balanceSingle(currency);
    }

    /**
     * This is pain
     * Created by martin on 2/16/16.
     *
     * @param currency Object
     * @return Data
     */
    public final BigDecimal balanceAll(final String currency) {
        return balance.balanceConverted(currency);
    }

    /**
     * This is pain
     * Created by martin on 2/16/16.
     *
     * @param amount   Object
     * @param currency Object
     * @param account  Object
     */
    public final void debit(final BigDecimal amount,
                            final String currency,
                            final Account account) {
        debitEx(amount, currency, account);
        balance.debit(currency, amount);
        account.credit(amount, currency);
    }

    /**
     * This is pain
     * Created by martin on 2/16/16.
     *
     * @param amount   Object
     * @param currency Object
     * @param account  Object
     */
    protected void debitEx(final BigDecimal amount,
                           final String currency,
                           final Account account) {
    }
}
