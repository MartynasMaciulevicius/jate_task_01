package lt.vu.mif.jate.task01.bank.exception;

/**
 * Babies cry
 * Created by martin on 2/16/16.
 */
public class IBANException extends Exception {

    /**
     * Babies cry
     * Created by martin on 2/16/16.
     */
    private String value;


    /**
     * Babies cry
     * Created by martin on 2/16/16.
     *
     * @param theValue Message
     * @param message  Message
     */
    public IBANException(final String theValue,
                         final String message) {
        super(message);
        this.value = theValue;
    }

    /**
     * Babies cry
     * Created by martin on 2/16/16.
     *
     * @param theValue  Message
     * @param message   Message
     * @param throwable Message
     */
    public IBANException(final String theValue,
                         final String message,
                         final Throwable throwable) {
        super(message, throwable);
        this.value = theValue;
    }

    /**
     * Sample text.
     *
     * @return Data
     */
    public final String getValue() {
        return value;
    }
}
