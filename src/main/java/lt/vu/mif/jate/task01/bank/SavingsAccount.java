/**
 * My package info
 */
package lt.vu.mif.jate.task01.bank;

import lt.vu.mif.jate.task01.bank.exception.AccountActionException;
import lt.vu.mif.jate.task01.bank.exception.IBANException;
import lt.vu.mif.jate.task01.bank.exception.NoFundsException;

import java.math.BigDecimal;

/**
 * I was here too.
 * Created by martin on 2/16/16.
 */
public class SavingsAccount extends CurrentAccount {
    /**
     * @param ibanNumber Number
     * @param bank       Number
     * @throws IBANException Data
     */
    public SavingsAccount(final IBANNumber ibanNumber, final Bank bank)
            throws IBANException {
        super(ibanNumber, bank);
    }

    /**
     * I was here too.
     *
     * @param amount   Amount
     * @param currency Amount
     * @throws AccountActionException Data
     */
    @Override
    public final void debitEx(final BigDecimal amount,
                              final String currency) {
        throw new AccountActionException();
    }

    /**
     * I was here too.
     *
     * @param amount   Amount
     * @param currency Amount
     * @param account  Account
     * @throws AccountActionException Data
     * @throws NoFundsException       Data
     */
    @Override
    public final void debitEx(final BigDecimal amount,
                              final String currency,
                              final Account account)
            throws NoFundsException, AccountActionException {
        throw new AccountActionException();
    }

}
