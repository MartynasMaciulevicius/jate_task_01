package lt.vu.mif.jate.task01.bank;


import lt.vu.mif.jate.task01.bank.rates.Rates;

/**
 * Yes, this is acceptable
 * Created by martin on 3/2/16.
 */
public class CurrencyRate {

    /**
     * Yes, this is acceptable
     * Created by martin on 3/2/16.
     */
    private final String title;

    /**
     * Yes, this is acceptable
     * Created by martin on 3/2/16.
     */
    private final Rates rates;

    /**
     * Yes, this is acceptable
     * Created by martin on 3/2/16.
     *
     * @param theTitle Title
     * @param theRates Rates
     */
    public CurrencyRate(final String theTitle, final Rates theRates) {
        this.title = theTitle;
        this.rates = theRates;
    }

    /**
     * Yes, this is acceptable
     * Created by martin on 3/2/16.
     *
     * @return Data
     */
    public final String getTitle() {
        return title;
    }

    /**
     * Yes, this is acceptable
     * Created by martin on 3/2/16.
     *
     * @return Data
     */
    public final Rates getRates() {
        return rates;
    }
}
