package lt.vu.mif.jate.task01.bank;

import lt.vu.mif.jate.task01.bank.exception.BankNotFoundException;
import lt.vu.mif.jate.task01.bank.exception.IBANException;
import lt.vu.mif.jate.task01.bank.exception.WrongAccountTypeException;
import lt.vu.mif.jate.task01.bank.files.Database;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Stream;

/**
 * This is a bicycle.
 * Created by martin on 2/16/16.
 */
public final class Banking {

    /**
     * This is a bicycle
     * Created by martin on 2/16/16.
     */
    private static Banking instance = new Banking();

    /**
     * This is a bicycle
     * Created by martin on 2/16/16.
     */
    private Converter converter = new Converter();

    /**
     * This is a bicycle
     * Created by martin on 2/16/16.
     */
    private Map<String, Map<Integer, Bank>> banks;

    /**
     * This is a bicycle
     * Created by martin on 2/16/16.
     */
    private Map<IBANNumber, Account> accounts = new TreeMap<>();

    /**
     * Copy-paste.
     */
    private Banking() {
        banks = createBankMap(
                Database.getInstance()
                        .getBanks());
    }

    /**
     * Best instance in the world.
     *
     * @return Best instance in the world
     */
    public static Banking getInstance() {
        return instance;
    }

    /**
     * Master rules the galaxy.
     *
     * @param masterMap Master is all
     * @param bank      Even this guy can not tell anything about it
     */
    private static void addBank(final Map<String, Map<Integer, Bank>> masterMap,
                                final Bank bank) {
        String country = bank.getLocale().getCountry();
        Map<Integer, Bank> maybeCountryMap = masterMap.get(country);
        if (maybeCountryMap == null) {
            maybeCountryMap = new HashMap<>();
            masterMap.put(country, maybeCountryMap);
        }
        maybeCountryMap.put(bank.getCode(), bank);
    }

    /**
     * Data.
     *
     * @return Maybe something meaningful
     */
    public Converter getConverter() {
        return converter;
    }

    /**
     * Nobody cares.
     *
     * @param country About the countries
     * @param id      anymore
     * @return I can bet you
     * @throws BankNotFoundException Data
     */
    public Bank getBank(final String country, final int id)
            throws BankNotFoundException {
        Map<Integer, Bank> maybeCountryMap = banks.get(country);
        if (maybeCountryMap == null) {
            throw new BankNotFoundException(country, id);
        }
        Bank maybeBank = maybeCountryMap.get(id);
        if (maybeBank == null) {
            throw new BankNotFoundException(country, id);
        }
        return maybeBank;
    }

    /**
     * This is love. This is life.
     *
     * @return Legacy
     */
    public Map<String, Map<Integer, Bank>> getBanks() {
        return createBankMap(banks.values()
                .stream()
                .map(Map::values)
                .flatMap(Collection::stream));
    }

    /**
     * Something smells bad.
     *
     * @param theBanks Banks
     * @return Nothing personal
     */
    private Map<String, Map<Integer, Bank>> createBankMap(
            final Stream<Bank> theBanks) {
        return theBanks.sequential()
                .collect(new HMSupplier(), Banking::addBank, (o, o2) -> {
                    throw new RuntimeException(
                            "Please put on the sequential method call."
                                    + " Too lazy to implement multithreading.");
                });
    }
    /**
     * Something smells bad
     *
     * @param accountNumber This is the thing
     * @return Nothing personal
     * @throws IBANException Data
     * @throws WrongAccountTypeException Data
     */
    /**
     * Something smells bad.
     *
     * @param accountNumber This is the thing
     * @return Nothing personal
     * @throws IBANException             Data
     * @throws WrongAccountTypeException Data
     */
    public Account getCurrentAccount(final String accountNumber)
            throws WrongAccountTypeException, IBANException {
        return getAccountByType(
                getIbanNumber(accountNumber),
                AccountFactory.Current);
    }

    /**
     * Something smells bad.
     *
     * @param accountNumber This is the thing
     * @return Nothing personal
     * @throws IBANException             Data
     * @throws WrongAccountTypeException Data
     */
    public Account getSavingsAccount(final String accountNumber)
            throws WrongAccountTypeException, IBANException {
        return getAccountByType(
                getIbanNumber(accountNumber),
                AccountFactory.Savings);
    }

    /**
     * Something smells bad.
     *
     * @param accountNumber This is the thing
     * @return Nothing personal
     * @throws WrongAccountTypeException Data
     * @throws IBANException             Data
     */
    public Account getCreditAccount(final String accountNumber)
            throws WrongAccountTypeException, IBANException {
        return getAccountByType(
                getIbanNumber(accountNumber),
                AccountFactory.Credit);
    }

    /**
     * Oh my lord.
     *
     * @param number         One plus one
     * @param accountFactory We make accounts
     * @return Data
     * @throws WrongAccountTypeException Data
     * @throws IBANException             Data
     */
    private Account getAccountByType(final IBANNumber number,
                                     final AccountFactory accountFactory)
            throws WrongAccountTypeException, IBANException {
        Account maybeAccount = accounts.get(number);
        if (maybeAccount != null) {
            if (!accountFactory.isAnInstance.apply(maybeAccount)) {
                throw new WrongAccountTypeException("Account type was "
                        + maybeAccount.getClass().getSimpleName());
            }
            return maybeAccount;
        }

        Account acc = accountFactory.accountSupplier.apply(
                number,
                getBank(number));
        accounts.put(number, acc);
        return acc;
    }

    /**
     * Just come at me.
     *
     * @param accNumber number
     * @return Nigger
     * @throws IBANException Data Randomness overload
     */
    private IBANNumber getIbanNumber(final String accNumber)
            throws IBANException {
        return new IBANNumber(accNumber);
    }

    /**
     * Little by little.
     *
     * @param number Number
     * @return Something
     * @throws IBANException Data Random stuff
     */
    private Bank getBank(final IBANNumber number) throws IBANException {
        Bank maybeBank;
        try {
            maybeBank = getBank(
                    number.getAccountCountry(),
                    number.getAccountBankCode()
            );
        } catch (BankNotFoundException e) {
            maybeBank = new Bank(
                    number.getAccountCountry(),
                    number.getAccountBankCode()
            );
            addBank(banks, maybeBank);
        }
        return maybeBank;
    }

    /**
     * The force is strong with this one.
     */
    private enum AccountFactory {
        /**
         * Data.
         */
        Current(
                CurrentAccount::new,
                account -> account.getClass().equals(CurrentAccount.class)),
        /**
         * Data.
         */
        Credit(
                CreditAccount::new,
                account -> account instanceof CreditAccount),
        /**
         * Data.
         */
        Savings(
                SavingsAccount::new,
                account -> account instanceof SavingsAccount);
        /**
         * Data.
         */
        private final IBANBiFunction<IBANNumber, Bank, Account> accountSupplier;
        /**
         * Data.
         */
        private final Function<Account, Boolean> isAnInstance;

        /**
         * Data.
         *
         * @param supplier        Data
         * @param theIsAnInstance Data
         */
        AccountFactory(final IBANBiFunction<IBANNumber, Bank, Account> supplier,
                       final Function<Account, Boolean> theIsAnInstance) {
            accountSupplier = supplier;
            this.isAnInstance = theIsAnInstance;
        }
    }

    /**
     * Agility.
     */
    private interface IBANBiFunction<T, U, R> {
        /**
         * Devastation.
         *
         * @param t T
         * @param u U
         * @return R
         * @throws IBANException Data
         */
        R apply(T t, U u) throws IBANException;
    }

    /**
     * This.
     */
    private static class HMSupplier
            implements Supplier<Map<String, Map<Integer, Bank>>> {
        /**
         * @return Hello
         */
        @Override
        public Map<String, Map<Integer, Bank>> get() {
            return new HashMap<>();
        }
    }

}
