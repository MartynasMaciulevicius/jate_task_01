package lt.vu.mif.jate.task01.bank;

import lt.vu.mif.jate.task01.bank.exception.IBANException;
import lt.vu.mif.jate.task01.bank.files.CountryRanges;
import lt.vu.mif.jate.task01.bank.files.Database;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.math.BigInteger;
import java.util.Locale;
import java.util.Optional;

/**
 * Hello world.
 * Created by martin on 2/16/16.
 */
public final class IBANNumber implements Comparable<IBANNumber> {
    /**
     * Sample text.
     */
    private static final String WRONG_FORMAT = "IBAN format wrong: ";
    /**
     * Sample text.
     */
    private final String country;
    /**
     * Sample text.
     */
    private final BigInteger accountNumber;
    /**
     * Sample text.
     */
    private final int accountBankCode;
    /**
     * Sample text.
     */
    private final String normalisedNumber;

    /**
     * Sample text.
     *
     * @param number Sample text
     * @throws IBANException Throws
     */
    public IBANNumber(final String number) throws IBANException {
        normalisedNumber = normalizeAccountNumber(number);
        if (!normalisedNumber.matches("^[A-Z]{2}[0-9]+$")) {
            throw new IBANException(
                    normalisedNumber,
                    WRONG_FORMAT + normalisedNumber);
        }

        country = normalisedNumber.substring(0, 2);

        CountryRanges bankIdNumberIndexes = getBankIdNumberIndexes(country)
                .orElseThrow(() -> new IBANException(
                        number,
                        "IBAN country not found: " + country));

        if (normalisedNumber.length() != bankIdNumberIndexes.getCodeLength()) {
            failOnLength(normalisedNumber, bankIdNumberIndexes.getCodeLength());
        }

        accountBankCode = Integer.parseInt(
                normalisedNumber.substring(
                        bankIdNumberIndexes.getBankCodeFrom(),
                        bankIdNumberIndexes.getBankCodeTo()));

        try {
            accountNumber = new BigInteger(
                    normalisedNumber.substring(
                            bankIdNumberIndexes.getAccCodeFrom(),
                            bankIdNumberIndexes.getAccCodeTo()));
        } catch (NumberFormatException nfe) {
            throw new IBANException(
                    normalisedNumber,
                    "IBAN format wrong: " + normalisedNumber,
                    nfe);
        }
    }

    /**
     * Lol.
     *
     * @return Data
     * @throws IBANException Throws
     */
    public int getAccountBankCode() throws IBANException {
        return accountBankCode;
    }

    @Override
    public int compareTo(final IBANNumber o) {
        return normalisedNumber.compareTo(o.normalisedNumber);
    }

    /**
     * Lol.
     *
     * @param countryCode Data
     * @return Data
     */
    private Optional<CountryRanges> getBankIdNumberIndexes(
            final String countryCode) {
        return Database.getInstance().getRanges(countryCode);
    }

    /**
     * Lol.
     *
     * @return Test
     * @throws IBANException Lol
     */
    public String getAccountCountry() throws IBANException {
        return country;
    }

    /**
     * Lol.
     *
     * @param theAccountNumber Data
     * @return Data
     */
    private String normalizeAccountNumber(final String theAccountNumber) {
        return theAccountNumber
                .replaceAll(" ", "")
                .toUpperCase(Locale.US);
    }

    /**
     * Lol.
     *
     * @return Data
     * @throws IBANException Lol
     */
    public BigInteger getAccountNumber() throws IBANException {
        return accountNumber;
    }

    /**
     * Lol.
     *
     * @return Data
     */
    public String getNormalisedNumber() {
        return normalisedNumber;
    }

    /**
     * Lol.
     *
     * @param theAccountNumber Number
     * @param expected         Number
     * @throws IBANException Lol
     */
    private void failOnLength(final String theAccountNumber,
                              final int expected) throws IBANException {
        throw new IBANException(
                theAccountNumber, "IBAN number length wrong: expected "
                + expected + ", got " + theAccountNumber.length());
    }

    /**
     * Lol.
     *
     * @param obj Object
     */
    @Override
    public boolean equals(final Object obj) {
        if (obj == null || obj.getClass() != getClass()) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        IBANNumber rhs = (IBANNumber) obj;
        return new EqualsBuilder()
                .append(normalisedNumber, rhs.normalisedNumber)
                .isEquals();
    }

    /**
     * Lol.
     *
     * @return Data
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(normalisedNumber.hashCode())
                .toHashCode();
    }

    /**
     * Lol.
     *
     * @return Data
     */
    @Override
    public String toString() {
        return normalisedNumber;
    }

}
