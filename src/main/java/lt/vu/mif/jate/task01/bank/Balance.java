package lt.vu.mif.jate.task01.bank;

import lt.vu.mif.jate.task01.bank.exception.NoFundsException;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;

/**
 * It does its job well
 * Created by martin on 2/20/16.
 */
public class Balance {
    /**
     * This is text.
     */
    private final Map<Currency, BigDecimal> amounts;
    /**
     * This is text.
     */
    private final Converter converter;
    /**
     * This is text.
     */
    private final Function<BigDecimal, BigDecimal> formatter =
            (num) -> num.setScale(2, BigDecimal.ROUND_HALF_UP);

    /**
     * It does its job well.
     *
     * @param theConverter Converter
     */
    public Balance(final Converter theConverter) {
        this.converter = theConverter;
        this.amounts = new HashMap<>();
    }

    /**
     * It does its job well.
     *
     * @param amount       Amount that you possess.
     * @param currencyCode Currency code.
     * @return Data
     */
    public final BigDecimal credit(final String currencyCode,
                                   final BigDecimal amount) {
        return formatter.apply(
                credit(
                        converter.getCurrency(currencyCode),
                        amount));
    }

    /**
     * @param amount   Amount that you possess.
     * @param currency Currency
     * @return Data
     */
    public final BigDecimal credit(final Currency currency,
                                   final BigDecimal amount) {
        try {
            return formatter.apply(
                    tamperAmount(
                            currency,
                            converter.validateDecimal(amount)));
        } catch (NoFundsException noMoreFunLeft) {
            throw new RuntimeException(
                    "Someone tampered with rules",
                    noMoreFunLeft);
        }
    }

    /**
     * @param currencyCode Code
     * @return Data
     */
    public final BigDecimal balanceSingle(final String currencyCode) {
        return formatter.apply(
                balanceSingle(
                        converter.getCurrency(
                                currencyCode)));
    }

    /**
     * @param currencyCode Code
     * @return Data
     */
    public final BigDecimal balanceConverted(final String currencyCode) {
        return formatter.apply(
                balanceConverted(
                        converter.getCurrency(
                                currencyCode)));
    }

    /**
     * @param currencyCode Code
     * @param amount       Amount that you possess.
     */
    public final void debit(final String currencyCode,
                            final BigDecimal amount) {
        debit(
                converter.getCurrency(currencyCode),
                amount);
    }

    /**
     * @param amount       Data
     * @param fromCurrency Data
     * @param toCurrency   Data
     */
    public final void convert(final BigDecimal amount,
                              final String fromCurrency,
                              final String toCurrency) {
        Currency from;
        Currency to;
        try {
            from = converter.getCurrency(fromCurrency);
            to = converter.getCurrency(toCurrency);
        } catch (IllegalArgumentException ill) {
            throw new NoFundsException();
        }
        convert(
                converter.validateDecimal(amount),
                from,
                to);
    }

    /**
     * @param currency   Data
     * @param difference Data
     * @return Data
     * @throws NoFundsException
     */
    private BigDecimal tamperAmount(final Currency currency,
                                    final BigDecimal difference) {
        BigDecimal amount = amounts.getOrDefault(currency, BigDecimal.ZERO).add(
                difference);
        if (difference.signum() == -1 && amount.signum() == -1) {
            throw new NoFundsException();
        }
        amounts.put(
                currency,
                amount);
        return amount;
    }

    /**
     * @param currency Data
     * @return Data
     */
    private BigDecimal balanceSingle(final Currency currency) {
        return amounts.getOrDefault(currency, new BigDecimal("0.00"));
    }

    /**
     * @param currency Currency
     * @return Data
     */
    private BigDecimal balanceConverted(final Currency currency) {
        return amounts.entrySet()
                .stream()
                .filter(e -> !(e.getValue().stripTrailingZeros().compareTo(
                        BigDecimal.ZERO) == 0))
                .map(entry -> {
                    if (Objects.equals(entry.getKey(), currency)) {
                        return entry.getValue();
                    } else {
                        return converter.convert(
                                entry.getValue(),
                                entry.getKey(),
                                currency);
                    }
                })
                .reduce(BigDecimal::add)

                .orElse(new BigDecimal("0.00"));
    }

    /**
     * @param currency Currency
     * @param amount   Amount that you possess.
     * @return Data
     */
    private BigDecimal debit(final Currency currency,
                             final BigDecimal amount) {
        return tamperAmount(
                currency,
                amount.negate());
    }

    /**
     * This function is totally safe. Use it.
     * Don't worry about race conditions, just smile and pray!
     *
     * @param theAmount       Amount that you possess.
     * @param theFromCurrency Convert from.
     * @param theToCurrency   Convert to.
     */
    private void convert(final BigDecimal theAmount,
                         final Currency theFromCurrency,
                         final Currency theToCurrency) {
        debit(
                theFromCurrency,
                theAmount);
        credit(
                theToCurrency,
                converter.convert(
                        theAmount,
                        theFromCurrency,
                        theToCurrency));
    }
}
