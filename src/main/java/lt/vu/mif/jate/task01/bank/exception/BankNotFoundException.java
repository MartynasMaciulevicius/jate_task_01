/**
 * Nigger stole my bike.
 */
package lt.vu.mif.jate.task01.bank.exception;

/**
 * FDP is the future.
 * Created by martin on 2/16/16.
 */
public final class BankNotFoundException extends Exception {
    /**
     * Sample text.
     */
    private final String country;
    /**
     * Sample text.
     */
    private final Integer code;

    /**
     * Sample text.
     *
     * @param countryCode Code
     * @param theCountry  Country
     */
    public BankNotFoundException(final String theCountry,
                                 final int countryCode) {
        super("Bank (" + theCountry + "-" + countryCode + ") was not found.");
        this.country = theCountry;
        this.code = countryCode;
    }

    /**
     * Sample text.
     *
     * @return Data
     */
    public String getCountry() {
        return country;
    }

    /**
     * Sample text.
     *
     * @return Data
     */
    public Integer getCode() {
        return code;
    }
}
