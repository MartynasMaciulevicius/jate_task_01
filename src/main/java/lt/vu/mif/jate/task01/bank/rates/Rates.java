/**
 * This is some package info.
 */
package lt.vu.mif.jate.task01.bank.rates;

import java.math.BigDecimal;

/**
 * Created by martin.
 */
public class Rates {
    /**
     * Created by martin on 3/2/16.
     */
    private final BigDecimal from;
    /**
     * Created by martin on 3/2/16.
     */
    private final BigDecimal to;

    /**
     * Created by martin on 3/2/16.
     *
     * @param theFrom Data
     * @param theTo   Data
     */
    public Rates(final BigDecimal theFrom, final BigDecimal theTo) {
        this.from = theFrom;
        this.to = theTo;
    }

    /**
     * Created by martin on 3/2/16.
     *
     * @return Data
     */
    public final BigDecimal getFrom() {
        return from;
    }

    /**
     * Created by martin on 3/2/16.
     *
     * @return Data
     */
    public final BigDecimal getTo() {
        return to;
    }
}
