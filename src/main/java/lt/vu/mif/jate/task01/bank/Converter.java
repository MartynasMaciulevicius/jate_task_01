package lt.vu.mif.jate.task01.bank;

import lt.vu.mif.jate.task01.bank.files.Database;
import lt.vu.mif.jate.task01.bank.rates.Rates;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Ninjas everywhere.
 * Created by martin on 2/16/16.
 */
public class Converter {

    /**
     * Data.
     */
    private final Optional<Function<BigDecimal, BigDecimal>> rounder =
            Optional.of((num) -> num.setScale(2, BigDecimal.ROUND_HALF_UP));

    /**
     * Data.
     */
    private final Currency base = getCurrency("EUR");

    /**
     * Data.
     */
    private final Map<Currency, Rates> ratesComparedToBase;

    /**
     * Data.
     */
    public Converter() {
        ratesComparedToBase =
                Database.getInstance().getRates()
                        .collect(
                                HashMap::new,
                                (map, triplet) -> {
                                    if (!isIsoStandardCurrency(
                                            triplet.getTitle())) {
                                        return;
                                    }
                                    map.put(
                                            getCurrency(triplet.getTitle()),
                                            triplet.getRates());
                                },
                                HashMap::putAll);
    }

    /**
     * Data.
     *
     * @return Data
     */
    public final Currency getBaseCurrency() {
        return base;
    }

    /**
     * Data.
     *
     * @param currCode Data
     * @return Data
     */
    public final BigDecimal getRateToBase(final String currCode) {
        return getRateToBase(getCurrency(currCode));
    }

    /**
     * Data.
     *
     * @param currency Data
     * @return Data
     */
    public final BigDecimal getRateToBase(final Currency currency) {
        return getRates(currency).getFrom();
    }

    /**
     * Data.
     *
     * @param currCode Data
     * @return Data
     */
    public final BigDecimal getRateFromBase(final String currCode) {
        return getRateFromBase(getCurrency(currCode));
    }

    /**
     * Data.
     *
     * @param currency Data
     * @return Data
     */
    public final BigDecimal getRateFromBase(final Currency currency) {
        return getRates(currency).getTo();
    }

    /**
     * Data.
     *
     * @param amount   Data
     * @param currCode Data
     * @return Data
     */
    public final BigDecimal toBase(final String amount, final String currCode) {
        return toBase(parseDecimal(amount), getCurrency(currCode), rounder);
    }

    /**
     * Data.
     *
     * @param amount   Data
     * @param currCode Data
     * @return Data
     */
    public final BigDecimal fromBase(final String amount,
                                     final String currCode) {
        return fromBase(
                validateDecimal(parseDecimal(amount)),
                getCurrency(currCode),
                rounder);
    }

    /**
     * Data.
     *
     * @param amount       Data
     * @param fromCurrCode Data
     * @param toCurrCode   Data
     * @return Data
     */
    public final BigDecimal convert(
            final String amount,
            final String fromCurrCode,
            final String toCurrCode) {
        return convert(
                validateDecimal(parseDecimal(amount)),
                getCurrency(fromCurrCode),
                getCurrency(toCurrCode));
    }

    /**
     * Data.
     */
    private static final int MAGIC_NUMBER = 189;

    /**
     * Data.
     *
     * @return Data
     */
    public final Set<Currency> getCurrencies() {
        return Currency.getAvailableCurrencies()
                .stream()
                .limit(MAGIC_NUMBER)
                .collect(
                        Collectors.toSet());
    }

    /**
     * Data.
     *
     * @param amount   Data
     * @param currFrom Data
     * @param currTo   Data
     * @return Data
     */
    public final BigDecimal convert(
            final BigDecimal amount,
            final Currency currFrom,
            final Currency currTo) {
        if (currFrom.equals(currTo)) {
            return amount;
        }
        return fromBase(
                toBase(
                        validateDecimal(amount),
                        currFrom,
                        Optional.empty()),
                currTo,
                rounder);
    }

    /**
     * Data.
     *
     * @param code Decimal
     * @return Data
     */
    public final Currency getCurrency(final String code) {
        switch (code) {
            case "G12":
            case "G32":
            case "E32":
                return base;
            default:
                return Currency.getInstance(code);
        }
    }

    /**
     * Data.
     *
     * @param currencyCode Decimal
     * @return Data
     */
    private boolean isIsoStandardCurrency(final String currencyCode) {
        return Currency.getAvailableCurrencies()
                .stream()
                .anyMatch(
                        currency -> currency.getCurrencyCode()
                                .equals(currencyCode));
    }

    /**
     * Data.
     *
     * @param currency Decimal
     * @return Data
     */
    private Rates getRates(final Currency currency) {
        if (ratesComparedToBase.containsKey(currency)) {
            return ratesComparedToBase.get(currency);
        }
        return new Rates(BigDecimal.ONE, BigDecimal.ONE);
    }

    /**
     * Data.
     *
     * @param input Decimal
     * @return Data
     */
    private BigDecimal parseDecimal(final String input) {
        try {
            return new BigDecimal(input);
        } catch (IllegalArgumentException ill) {
            throw new NumberFormatException();
        }
    }

    /**
     * Data.
     *
     * @param decimal Decimal
     * @return Data
     */
    public final BigDecimal validateDecimal(final BigDecimal decimal) {
        if (decimal.scale() > 2 || decimal.signum() == -1) {
            throw new NumberFormatException();
        }
        return decimal;
    }

    /**
     * Data.
     *
     * @param amount        Amount
     * @param currency      Currency
     * @param rateExtractor Rate extractor
     * @param postProcessor PostProcessor
     * @return Data
     */
    private BigDecimal convertAndRound(
            final BigDecimal amount,
            final Currency currency,
            final Function<Rates, BigDecimal> rateExtractor,
            final Optional<Function<BigDecimal, BigDecimal>> postProcessor) {
        return postProcessor
                .orElse(same -> same)
                .apply(
                        amount.multiply(
                                rateExtractor.apply(
                                        getRates(currency))));
    }

    /**
     * Data.
     *
     * @param amount        Amount
     * @param currency      Currency
     * @param postProcessor PostProcessor
     * @return Data
     */
    private BigDecimal toBase(final BigDecimal amount,
                              final Currency currency,
                              final Optional<Function<BigDecimal, BigDecimal>>
                                      postProcessor) {
        return convertAndRound(
                validateDecimal(amount),
                currency,
                Rates::getFrom,
                postProcessor);
    }

    /**
     * Data.
     *
     * @param amount        Amount
     * @param currency      Currency
     * @param postProcessor PostProcessor
     * @return Data
     */
    private BigDecimal fromBase(final BigDecimal amount,
                                final Currency currency,
                                final Optional<Function<BigDecimal, BigDecimal>>
                                        postProcessor) {
        return convertAndRound(
                amount,
                currency,
                Rates::getTo,
                postProcessor);
    }

}
