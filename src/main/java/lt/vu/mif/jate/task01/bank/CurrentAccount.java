/**
 * Sample text
 */
package lt.vu.mif.jate.task01.bank;

import lt.vu.mif.jate.task01.bank.exception.IBANException;

import java.math.BigDecimal;

/**
 * Sample text.
 * Created by martin on 2/16/16.
 */
public class CurrentAccount extends Account {
    /**
     * Sample text.
     *
     * @param ibanNumber Sample test
     * @param bank       Sample test
     * @throws IBANException Throws
     */
    public CurrentAccount(final IBANNumber ibanNumber, final Bank bank)
            throws IBANException {
        super(ibanNumber, bank);
    }

    /**
     * Data.
     *
     * @param amount   Amount
     * @param currency Currency
     * @return Data
     */
    @Override
    protected void creditEx(final BigDecimal amount,
                            final String currency) {
    }

    /**
     * Data.
     *
     * @param amount   Amount
     * @param currency Currency
     * @return Data
     */

    @Override
    protected void debitEx(final BigDecimal amount,
                           final String currency) {
    }
}
