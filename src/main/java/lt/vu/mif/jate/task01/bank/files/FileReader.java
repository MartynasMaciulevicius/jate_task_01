package lt.vu.mif.jate.task01.bank.files;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * A class.
 * Created by martin on 2/22/16.
 */
public class FileReader {

    /**
     * This is Swish.
     *
     * @param fileName File
     * @return Data
     */
    public final Optional<Stream<String>> read(final String fileName) {
        Collection<String> strings = null;
        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(new File(
                                ClassLoader
                                        .getSystemResource(fileName)
                                        .getFile())),
                        Charset.defaultCharset()))
        ) {
            strings = reader
                    .lines()
                    .collect(
                            Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Optional.ofNullable(strings)
                .map(Collection::stream);
    }
}
