package lt.vu.mif.jate.task01.bank.files;

/**
 * Lol.
 */
public class CountryRanges {
    /**
     * Lol.
     */
    private final int bankCodeFrom;
    /**
     * Lol.
     */
    private final int bankCodeTo;
    /**
     * Lol.
     */
    private final int accCodeFrom;
    /**
     * Lol.
     */
    private final int accCodeTo;
    /**
     * Lol.
     */
    private final int codeLength;

    /**
     * Lol.
     *
     * @param theAccCodeFrom  Param
     * @param theAccCodeTo    Param
     * @param theBankCodeFrom Param
     * @param theBankCodeTo   Param
     * @param length          Param
     */
    public CountryRanges(
            final int theBankCodeFrom,
            final int theBankCodeTo,
            final int theAccCodeFrom,
            final int theAccCodeTo,
            final int length) {
        this.bankCodeFrom = theBankCodeFrom;
        this.bankCodeTo = theBankCodeTo;
        this.accCodeFrom = theAccCodeFrom;
        this.accCodeTo = theAccCodeTo;
        this.codeLength = length;
    }

    /**
     * Lol.
     *
     * @return Test
     */
    public final int getAccCodeFrom() {
        return accCodeFrom;
    }

    /**
     * Lol.
     *
     * @return Test
     */
    public final int getBankCodeTo() {
        return bankCodeTo;
    }

    /**
     * Lol.
     *
     * @return Test
     */
    public final int getBankCodeFrom() {
        return bankCodeFrom;
    }

    /**
     * Lol.
     *
     * @return Test
     */
    public final int getCodeLength() {
        return codeLength;
    }

    /**
     * Lol.
     *
     * @return Test
     */
    public final int getAccCodeTo() {
        return accCodeTo;
    }
}
