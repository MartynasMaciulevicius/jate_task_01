package lt.vu.mif.jate.task01.bank;

import lt.vu.mif.jate.task01.bank.exception.AccountActionException;
import lt.vu.mif.jate.task01.bank.exception.IBANException;

import java.math.BigDecimal;

/**
 * I was here.
 * Created by martin on 2/16/16.
 */
public class CreditAccount extends Account {
    /**
     * Light.
     */
    private boolean hasCreditedOnce = false;

    /**
     * This is life.
     *
     * @param bank       Bank
     * @param ibanNumber IBAN Number
     * @throws IBANException Data
     */
    public CreditAccount(final IBANNumber ibanNumber, final Bank bank)
            throws IBANException {
        super(ibanNumber, bank);
    }

    /**
     * This is life.
     *
     * @param amount   Amount
     * @param currency Currency
     * @throws AccountActionException Data
     */
    @Override
    public final void creditEx(final BigDecimal amount, final String currency)
            throws AccountActionException {
        if (hasCreditedOnce) {
            throw new AccountActionException();
        }
        hasCreditedOnce = true;
    }

    /**
     * Data.
     *
     * @param amount   Amount
     * @param currency Currency
     * @return Data
     */

    @Override
    protected final void debitEx(final BigDecimal amount,
                                 final String currency) {
    }
}
