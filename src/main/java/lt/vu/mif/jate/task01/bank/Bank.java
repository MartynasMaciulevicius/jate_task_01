package lt.vu.mif.jate.task01.bank;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.Locale;

/**
 * Data.
 */
public class Bank {

    /**
     * Data.
     */
    private final Locale locale;
    /**
     * Data.
     */
    private final Integer code;
    /**
     * Data.
     */
    private final String bicCode;
    /**
     * Data.
     */
    private final String name;
    /**
     * Data.
     */
    private final String address;

    /**
     * Doc.
     *
     * @param country    Country
     * @param theCode    Code
     * @param theBicCode BiCode
     * @param nameName   Name
     * @param theAddress Address
     */
    public Bank(final String country,
                final int theCode,
                final String theBicCode,
                final String nameName,
                final String theAddress) {
        this.locale = new Locale("lt.vu.mif.jate.bank.unknownLocale", country);
        this.code = theCode;
        this.bicCode = theBicCode;
        this.name = nameName;
        this.address = theAddress;
    }

    /**
     * Doc.
     *
     * @param theCountry Country
     * @param theCode    Code
     * @param theBicCode BiCode
     * @param theName    Name
     */
    public Bank(final String theCountry,
                final int theCode,
                final String theBicCode,
                final String theName) {
        this(
                theCountry,
                theCode,
                theBicCode,
                theName,
                null); // Tests that want null? This is pain.
    }

    /**
     * Doc.
     *
     * @param theCountry Country
     * @param theCode    Code
     * @param theBicCode BiCode
     */
    public Bank(final String theCountry,
                final int theCode,
                final String theBicCode) {
        this(theCountry, theCode, theBicCode, null);
    }

    /**
     * Doc.
     *
     * @param theCountry Country
     * @param theCode    Code
     */
    public Bank(final String theCountry, final int theCode) {
        this(theCountry, theCode, null);
    }

    /**
     * Doc.
     *
     * @return Data
     */
    public final Locale getLocale() {
        return locale;
    }

    /**
     * Doc.
     *
     * @return Data
     */
    public final Integer getCode() {
        return code;
    }

    /**
     * Doc.
     *
     * @return Data
     */
    public final String getBicCode() {
        return bicCode;
    }

    /**
     * Doc.
     *
     * @return Data
     */
    public final String getName() {
        return name;
    }

    /**
     * Doc.
     *
     * @return Data
     */
    public final String getAddress() {
        return address;
    }

    /**
     * Doc.
     *
     * @param obj Object
     * @return Data
     */
    @Override
    public final boolean equals(final Object obj) {
        if (!(obj instanceof Bank)) {
            return false;
        }

        Bank bank = (Bank) obj;
        return locale.equals(bank.locale) && code.equals(bank.code);
    }

    /**
     * Doc.
     *
     * @return Data
     */
    @Override
    public final int hashCode() {
        return new HashCodeBuilder()
                .append(locale.hashCode())
                .append(code.hashCode())
                .build();
    }

    /**
     * Doc.
     *
     * @return Data
     */
    @Override
    public final String toString() {
        if (name != null) {
            return name;
        }
        String out = "Bank#" + code;
        if (bicCode != null) {
            out = out + " (" + bicCode + ")";
        }
        if (locale != null) {
            out = out + ", " + locale.getDisplayCountry();
        }
        return out;
    }
}
