package lt.vu.mif.jate.task01.bank.exception;

/**
 * Just look at that.
 * Created by martin on 2/16/16.
 */
public final class WrongAccountTypeException extends Exception {
    /**
     * Just look at that.
     *
     * @param message Message
     */
    public WrongAccountTypeException(final String message) {
        super(message);
    }
}
