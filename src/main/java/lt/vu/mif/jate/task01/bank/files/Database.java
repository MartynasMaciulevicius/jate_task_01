package lt.vu.mif.jate.task01.bank.files;

import lt.vu.mif.jate.task01.bank.Bank;
import lt.vu.mif.jate.task01.bank.CurrencyRate;
import lt.vu.mif.jate.task01.bank.rates.Rates;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Data is all around you
 * Created by martin on 2/22/16.
 */
public class Database {

    /**
     * The world is watching you.
     */
    private static final Database ME = new Database();

    /**
     * The world is watching you.
     *
     * @return Data
     */
    public static Database getInstance() {
        return ME;
    }

    /**
     * The world is watching you.
     */
    private final FileReader reader = new FileReader();

    /**
     * You must believe in your dreams.
     *
     * @param fileName This is your destiny
     * @return Data
     */
    private Optional<Collection<String>> getFileData(final String fileName) {
        return reader
                .read(fileName)
                .map(stringStream -> stringStream.collect(Collectors.toList()));
    }

    /**
     * This is awesome.
     */
    private static final class CountryRangeWithId {
        /**
         * This is awesome.
         */
        private final String id;

        /**
         * This is awesome.
         */
        private final CountryRanges data;

        /**
         * This is awesome.
         *
         * @param theData Data
         * @param theId   Identifier
         */
        private CountryRangeWithId(final String theId,
                                   final CountryRanges theData) {
            this.id = theId;
            this.data = theData;
        }
    }

    /**
     * Don't fail the will.
     *
     * @param identifier Yes
     * @return Data will guide you
     */
    public final Optional<CountryRanges> getRanges(final String identifier) {
        if (countryRanges == null) {
            countryRanges = getFileData("banking/iban.txt")
                    .map(Collection::stream)
                    .map(stringStream -> stringStream
                            .map(s -> s.replaceAll(" ", ""))
                            .map(s -> {

                                /** Test. */
                                int countryCodeStart = s.indexOf(':') + 1;

                                /**Test. */
                                String countryCode = s.substring(
                                        countryCodeStart,
                                        countryCodeStart + 2);

                                /** Test. */
                                int bankCodeFrom = s.indexOf(
                                        'b',
                                        countryCodeStart) - countryCodeStart;

                                /** Test. */
                                int bankCodeTo = s.lastIndexOf('b')
                                        - countryCodeStart + 1;

                                /** Test. */
                                int accountCodeFrom = s.indexOf(
                                        'c',
                                        countryCodeStart) - countryCodeStart;

                                /** Test. */
                                int accountCodeTo = s.lastIndexOf('c')
                                        - countryCodeStart + 1;

                                /** Test. */
                                return new CountryRangeWithId(
                                        countryCode,
                                        new CountryRanges(
                                                bankCodeFrom,
                                                bankCodeTo,
                                                accountCodeFrom,
                                                accountCodeTo,
                                                s.length() - countryCodeStart));
                            })
                    )
                    .orElse(Stream.empty())
                    .collect(
                            TreeMap::new,
                            (tupleMap, stringCountryRangesTuple) ->
                                    tupleMap.put(
                                            stringCountryRangesTuple.id,
                                            stringCountryRangesTuple.data),
                            TreeMap::putAll);
        }
        return Optional.ofNullable(countryRanges).map(r -> r.get(identifier));
    }

    /**
     * Countries mapped to their data.
     * Pretty impressive, this code is 46 lines long. And it is static.
     * *EVIL LAUGH*
     */
    private Map<String, CountryRanges> countryRanges;

    /**
     * Definitely not a hardcode.
     */
    private final Collection<Integer> bankParseArgs = Arrays.asList(3, 2, 0, 1);

    /**
     * Don't fail the will.
     *
     * @return Data will guide you
     */
    public final Stream<Bank> getBanks() {
        return getFileData("banking/banks.txt")
                .map(Collection::stream)
                .map(stringStream -> stringStream
                        .map(s -> {

                            /** Test. */
                            String[] parts = s.split(":");
                            Iterator<Integer> args = bankParseArgs.iterator();

                            return new Bank(
                                    "LT",
                                    Integer.parseInt(parts[args.next()]),
                                    parts[args.next()],
                                    parts[args.next()],
                                    parts[args.next()]
                            );
                        })
                )
                .orElse(Stream.empty())
                .collect(Collectors.toList())
                .stream();
    }

    /**
     * Don't fail the will.
     *
     * @return Data will guide you
     */
    public final Stream<CurrencyRate> getRates() {
        return getFileData("banking/rates.txt")
                .map(Collection::stream)
                .map(stringStream -> stringStream
                        .map(s -> {

                            /** Test. */
                            String[] parts = s.split(":");

                            return new CurrencyRate(
                                    parts[0],
                                    new Rates(
                                            new BigDecimal(parts[1]),
                                            new BigDecimal(parts[2])));
                        })
                )
                .orElse(Stream.empty())
                .collect(Collectors.toList())
                .stream();
    }

}
